use crate::{analysis::blocked_in, *};
use std::collections::HashSet;

#[test]
fn test_stage_1_analysis() {
    // Test stage 1 analysis
    {
        let mut date = get_date();

        // New bridge info
        let mut bridge_info = BridgeInfo::new([0; 20], &String::default());

        bridge_info
            .info_by_country
            .insert("ru".to_string(), BridgeCountryInfo::new(date));
        let analyzer = analysis::NormalAnalyzer::new(5, 0.25);
        let confidence = 0.95;

        let mut blocking_countries = HashSet::<String>::new();

        // No data today
        assert_eq!(
            blocked_in(&analyzer, &bridge_info, confidence, date, 30, 30),
            blocking_countries
        );

        // 1 connection, 0 negative reports
        date += 1;
        bridge_info.info_by_country.get_mut("ru").unwrap().add_info(
            BridgeInfoType::BridgeIps,
            date,
            8,
        );
        assert_eq!(
            blocked_in(&analyzer, &bridge_info, confidence, date, 30, 30),
            blocking_countries
        );

        // 0 connections, 0 negative reports
        date += 1;
        bridge_info.info_by_country.get_mut("ru").unwrap().add_info(
            BridgeInfoType::BridgeIps,
            date,
            0,
        );
        assert_eq!(
            blocked_in(&analyzer, &bridge_info, confidence, date, 30, 30),
            blocking_countries
        );

        // 0 connections, 1 negative report
        // (exceeds scaled threshold)
        date += 1;
        bridge_info.info_by_country.get_mut("ru").unwrap().add_info(
            BridgeInfoType::NegativeReports,
            date,
            1,
        );
        blocking_countries.insert("ru".to_string());
        assert_eq!(
            blocked_in(&analyzer, &bridge_info, confidence, date, 30, 30),
            blocking_countries
        );
    }

    {
        let mut date = get_date();

        // New bridge info
        let mut bridge_info = BridgeInfo::new([0; 20], &String::default());

        bridge_info
            .info_by_country
            .insert("ru".to_string(), BridgeCountryInfo::new(date));
        let analyzer = analysis::NormalAnalyzer::new(5, 0.25);
        let confidence = 0.95;

        let mut blocking_countries = HashSet::<String>::new();

        // No data today
        assert_eq!(
            blocked_in(&analyzer, &bridge_info, confidence, date, 30, 30),
            blocking_countries
        );

        // 1 connection, 1 negative report
        date += 1;
        bridge_info.info_by_country.get_mut("ru").unwrap().add_info(
            BridgeInfoType::BridgeIps,
            date,
            8,
        );
        bridge_info.info_by_country.get_mut("ru").unwrap().add_info(
            BridgeInfoType::NegativeReports,
            date,
            1,
        );
        assert_eq!(
            blocked_in(&analyzer, &bridge_info, confidence, date, 30, 30),
            blocking_countries
        );

        // 8 connections, 2 negative reports
        date += 1;
        bridge_info.info_by_country.get_mut("ru").unwrap().add_info(
            BridgeInfoType::BridgeIps,
            date,
            8,
        );
        bridge_info.info_by_country.get_mut("ru").unwrap().add_info(
            BridgeInfoType::NegativeReports,
            date,
            2,
        );
        assert_eq!(
            blocked_in(&analyzer, &bridge_info, confidence, date, 30, 30),
            blocking_countries
        );

        // 8 connections, 3 negative reports
        // (exceeds scaled threshold)
        date += 1;
        bridge_info.info_by_country.get_mut("ru").unwrap().add_info(
            BridgeInfoType::BridgeIps,
            date,
            8,
        );
        bridge_info.info_by_country.get_mut("ru").unwrap().add_info(
            BridgeInfoType::NegativeReports,
            date,
            3,
        );
        blocking_countries.insert("ru".to_string());
        assert_eq!(
            blocked_in(&analyzer, &bridge_info, confidence, date, 30, 30),
            blocking_countries
        );
    }

    {
        let mut date = get_date();

        // New bridge info
        let mut bridge_info = BridgeInfo::new([0; 20], &String::default());

        bridge_info
            .info_by_country
            .insert("ru".to_string(), BridgeCountryInfo::new(date));
        let analyzer = analysis::NormalAnalyzer::new(5, 0.25);
        let confidence = 0.95;

        let mut blocking_countries = HashSet::<String>::new();

        // 24 connections, 5 negative reports
        bridge_info.info_by_country.get_mut("ru").unwrap().add_info(
            BridgeInfoType::BridgeIps,
            date,
            24,
        );
        bridge_info.info_by_country.get_mut("ru").unwrap().add_info(
            BridgeInfoType::NegativeReports,
            date,
            5,
        );
        assert_eq!(
            blocked_in(&analyzer, &bridge_info, confidence, date, 30, 30),
            blocking_countries
        );

        // 24 connections, 6 negative reports
        // (exceeds max threshold)
        date += 1;
        bridge_info.info_by_country.get_mut("ru").unwrap().add_info(
            BridgeInfoType::BridgeIps,
            date,
            24,
        );
        bridge_info.info_by_country.get_mut("ru").unwrap().add_info(
            BridgeInfoType::NegativeReports,
            date,
            6,
        );
        blocking_countries.insert("ru".to_string());
        assert_eq!(
            blocked_in(&analyzer, &bridge_info, confidence, date, 30, 30),
            blocking_countries
        );
    }
}
